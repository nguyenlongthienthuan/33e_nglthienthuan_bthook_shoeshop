import React, { memo, useMemo, useState } from 'react'
import { dataBuy, dataShoes } from './data'
import ItemShoe from './ItemShoe'

 function ListItemShoes({handelAddToCart}) {
  
 let renderList=useMemo(()=>{return dataShoes.map((item,index)=>{return <ItemShoe key={index} shoe={item} handelAddToCart={handelAddToCart}></ItemShoe>})},[])

  return (
    <div className='grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4  gap-3 container mx-auto pt-14'>
        {renderList}
    </div>
  )
}
export default memo(ListItemShoes)
// let [ListBuy,setListBuy]=useState(dataBuy)
//   let handelAddToCart=(shoe)=>{
     
//      let index=ListBuy.findIndex((item)=>{return item.id==shoe.id})
//      index==-1?setListBuy([...ListBuy,{...shoe,soLuong:1}]):
//       setListBuy([...ListBuy.fill({...shoe,soLuong:ListBuy[index].soLuong+1},index,index+1)])
//   }
