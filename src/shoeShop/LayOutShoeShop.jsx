import React, { memo, useCallback, useState } from 'react'
import ListItemShoes from './ListItemShoes'
import ShoppingCart from './ShoppingCart'

export default function LayOutShoeShop() {
  let [ListBuy,setListBuy]=useState([])
  let handelAddToCart=useCallback((shoe)=>{
    let index=ListBuy.findIndex((item)=>{return item.id==shoe.id})
    index==-1?setListBuy(ListBuy=[...ListBuy,{...shoe,soLuong:1}]):
     setListBuy(ListBuy=[...ListBuy.fill({...shoe,soLuong:ListBuy[index].soLuong+1},index,index+1)])
 },[])
 let handelRemoveCart=useCallback((idShoe)=>{
  let index=ListBuy.findIndex((item)=>{return idShoe==item.id});
  let clone=[...ListBuy];
  clone.splice(index, 1)
     setListBuy(ListBuy=clone)
 },[]);
 let handelChangeQuantity=useCallback((idShoe,payload)=>{
   let index=ListBuy.findIndex((item)=>{return idShoe==item.id})
   let clone=[...ListBuy];
    clone[index].soLuong+=payload;
   setListBuy(ListBuy=clone);
 },[])
console.log('layout');
  return (
    <div>
      <ShoppingCart dataBuy={ListBuy}  handelChangeQuantity={handelChangeQuantity} handelRemoveCart={handelRemoveCart}></ShoppingCart>
      <ListItemShoes handelAddToCart={handelAddToCart}></ListItemShoes>
    </div>
  )
}
 