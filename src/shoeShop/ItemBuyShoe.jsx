import React, { memo } from 'react'

 function ItemBuyShoe({handelChangeQuantity,handelRemoveCart,shoe}) {
     console.log('itembuy');
    return (
        <li className="flex py-6">
        <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
          <img src={shoe.image} alt="Front of satchel with blue canvas body, black straps and handle, drawstring top, and front zipper pouch." className="h-full w-full object-cover object-center" />
        </div>
        <div className="ml-4 flex flex-1 flex-col">
          <div>
            <div className="flex justify-between text-base font-medium text-gray-900">
              <h3>
                <a href="#">{shoe.name}</a>
              </h3>
              <p className="ml-4">{shoe.price*shoe.soLuong}$</p>
            </div>
            <p className="mt-1 text-sm text-gray-500">{shoe.shortDescription}</p>
          </div>
          <div className="flex flex-1 items-end justify-between text-sm">
           <div className='flex'>
            <button onClick={()=>{handelChangeQuantity(shoe.id,-1)}} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-0.5 px-1.5 mx-1 rounded">-</button>
            <p className="text-gray-500">{shoe.soLuong<=0?handelRemoveCart(shoe.id):shoe.soLuong}</p>
            <button onClick={()=>{handelChangeQuantity(shoe.id,1)}} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-0.5 px-1 mx-1 rounded">+</button>
           </div>
            <div className="flex">
              <button onClick={()=>{handelRemoveCart(shoe.id)}} type="button" className="font-medium text-indigo-600 hover:text-indigo-500">Remove</button>
            </div>
          </div>
        </div>
      </li>
      )
  
}
export default memo(ItemBuyShoe);