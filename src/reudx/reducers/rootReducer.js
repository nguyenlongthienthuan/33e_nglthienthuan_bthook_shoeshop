import { combineReducers } from "redux";
import { reducerShoeShop } from "./reducerShoeShop";

export const rootReducer = combineReducers({
  reducerShoeShop,
});
