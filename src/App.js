import logo from "./logo.svg";
import "./App.css";
import LayOutShoeShop from "./shoeShop/LayOutShoeShop";

function App() {
  return (
    <div className="App">
      <LayOutShoeShop></LayOutShoeShop>
    </div>
  );
}

export default App;
